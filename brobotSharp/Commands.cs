﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace brobotSharp
{
    public class UngroupedCommands
    {
        [Command("ping")]
        [Description("Example ping command")]
        [Aliases("pong")]
        public async Task Ping(CommandContext context)
        {
            await context.TriggerTypingAsync();
            var emoji = DiscordEmoji.FromName(context.Client, ":ping_pong:");
            await context.RespondAsync($"{emoji} Pong! {context.Client.Ping} ms");
        }

        [Command("greet")]
        [Description("Say hello to the tagged user")]
        [Aliases("sayhi")]
        public async Task Greet(CommandContext context, [Description("Say hello to the user")] DiscordMember member)
        {
            await context.TriggerTypingAsync();
            await context.RespondAsync($"Hello, {member.Mention}!");
        }

        [Command("insult")]
        [Description("Insults the tagged user")]
        [Aliases("diss")]
        public async Task Insult(CommandContext context, [Description("Insults the tagged user")] DiscordMember member)
        {
            await context.TriggerTypingAsync();
            Console.WriteLine(member.Id);
            Console.WriteLine(member.Nickname);
            Console.WriteLine(member.DisplayName);
            await context.RespondAsync($"{member.Mention} is a douche!");
        }
    }
}
