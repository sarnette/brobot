﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Newtonsoft.Json;

namespace brobotSharp
{
    public class Program
    {
        public DiscordClient Client { get; set; }
        public CommandsNextModule Commands { get; set; }

        public static void Main(string[] args)
        {
            var prog = new Program();
            prog.RunBotAsync().GetAwaiter().GetResult();
        }

        public async Task RunBotAsync()
        {
            //Read in config.json file.
            var json = "";
            using (var fileStream = File.OpenRead("config.json"))
            using (var streamReader = new StreamReader(fileStream, new UTF8Encoding(false)))
                json = await streamReader.ReadToEndAsync();

            //Convert json into usable values.
            var configJson = JsonConvert.DeserializeObject<ConfigJson>(json);

            var discordConfig = new DiscordConfiguration
            {
                Token = configJson.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                LogLevel = LogLevel.Debug,
                UseInternalLogHandler = true
            };

            //Create the bot object with the json configuration.
            this.Client = new DiscordClient(discordConfig);

            //Call events to know how setup went.
            this.Client.Ready += this.Client_Ready;
            this.Client.GuildAvailable += this.Client_GuildAvailable;
            this.Client.ClientErrored += this.Client_ClientError;

            //Create command configuration variable.
            var commandConfig = new CommandsNextConfiguration
            {
                StringPrefix = configJson.CommandPrefix,
                EnableDms = true,
                EnableMentionPrefix = true
            };

            //Create commands object using commandConfig.
            this.Commands = this.Client.UseCommandsNext(commandConfig);

            //Call events for commands.
            this.Commands.CommandExecuted += this.Commands_CommandExecuted;
            this.Commands.CommandErrored += this.Commands_CommandErrored;

            //Register commands.
            //this.Commands.RegisterCommands<>();
            //this.Commands.RegisterCommands<>();
            await this.Client.ConnectAsync();
            await Task.Delay(-1);
        }

        private Task Client_Ready (ReadyEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "BroBot", "BroBot is online and ready.", DateTime.Now);
            return Task.CompletedTask;
        }

        private Task Client_GuildAvailable (GuildCreateEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "BroBot", $"Guild available: {e.Guild.Name}", DateTime.Now);
            return Task.CompletedTask;
        }

        private Task Client_ClientError (ClientErrorEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Error, "BroBot", $"Exception occurred: {e.Exception.GetType()}", DateTime.Now);
            return Task.CompletedTask;
        }

        private Task Commands_CommandExecuted (CommandExecutionEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Info, "BroBot", $"{e.Context.User.Username} ran a command successfully.", DateTime.Now);
            return Task.CompletedTask;
        }

        private async Task Commands_CommandErrored(CommandErrorEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Error, "BroBot", $"{e.Context.User.Username} failed executing '{e.Command?.QualifiedName ?? "<unknown command>"}' with error: {e.Exception.GetType()}: {e.Exception.Message ?? "<no message>"}", DateTime.Now);

            if (e.Exception is ChecksFailedException ex)
            {
                var embed = new DiscordEmbedBuilder
                {
                    Title = "Access denied",
                    Description = "You do not have permission to execute this command.",
                    Color = new DiscordColor(0xFF0000)
                };

                await e.Context.RespondAsync("", embed: embed);
            }
        }

    }
    public struct ConfigJson
    {
        [JsonProperty("token")]
        public string Token { get; private set; }

        [JsonProperty("prefix")]
        public string CommandPrefix { get; private set; }
    }
}
